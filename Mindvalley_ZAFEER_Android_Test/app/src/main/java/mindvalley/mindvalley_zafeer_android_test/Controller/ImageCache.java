package mindvalley.mindvalley_zafeer_android_test.Controller;

import android.graphics.Bitmap;
import android.util.LruCache;
import com.android.volley.toolbox.ImageLoader;

/**
 * Created by Zaf on 10/10/2016.
 */

public class ImageCache extends LruCache<String,Bitmap> implements ImageLoader.ImageCache {


    public static int getDefaultLruCacheSize(){
        final int maxMemory = (int)(Runtime.getRuntime().maxMemory() /1024);
        final int cacheSize = maxMemory /8;
        return cacheSize;
    }

    public ImageCache() {
        this(getDefaultLruCacheSize());
    }

    public ImageCache(int sizeInKb) {
        super(sizeInKb);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        return value.getRowBytes() * value.getHeight() / 1024;
    }

    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        put(url,bitmap);
    }
}
