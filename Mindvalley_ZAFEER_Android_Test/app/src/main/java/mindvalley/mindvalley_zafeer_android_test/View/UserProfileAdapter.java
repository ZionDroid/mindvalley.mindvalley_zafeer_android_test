package mindvalley.mindvalley_zafeer_android_test.View;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import java.util.List;

import mindvalley.mindvalley_zafeer_android_test.Controller.User_profile;
import mindvalley.mindvalley_zafeer_android_test.Controller.VolleyController;

import mindvalley.mindvalley_zafeer_android_test.Model.POJO;
import mindvalley.mindvalley_zafeer_android_test.R;

/**
 * Created by Zaf on 12/10/2016.
 */

public class UserProfileAdapter extends RecyclerView.Adapter<UserProfileAdapter.MyHolder> {

    private Context mContext;
    private List<POJO> pojoList;
    private ImageLoader.ImageContainer mImageContainer;


    public class MyHolder extends RecyclerView.ViewHolder{
        public TextView title, count;
        public ImageView thumbnail, overflow;


        public MyHolder(View itemView) {
            super(itemView);

            title = (TextView)itemView.findViewById(R.id.title);
            count = (TextView)itemView.findViewById(R.id.count);
            thumbnail = (ImageView)itemView.findViewById(R.id.pro_pic_thumb);
            overflow = (ImageView)itemView.findViewById(R.id.overflow);
        }
    }

    public UserProfileAdapter(Context context, List<POJO> pojoList){
        this.mContext = context;
        this.pojoList = pojoList;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_profile_gallery,parent,false);

        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyHolder holder, int position) {

        ImageLoader imageLoader = VolleyController.getInstance().getImageLoader();
        final POJO thumbnails = pojoList.get(position);
        final POJO img = pojoList.get(position);
         holder.title.setText(thumbnails.getUser().getUsername());
         holder.count.setText(thumbnails.getLikes() + "likes");

        final String TAG = User_profile.class.getSimpleName();


        imageLoader.get(img.getUrls().getThumb(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null){
                    holder.thumbnail.setImageBitmap(response.getBitmap());
                }else if(img.getUrls().getThumb() == null){
                    mImageContainer.cancelRequest();
                    mImageContainer = null;
                }
            }
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
                holder.thumbnail.setImageResource(R.drawable.warning);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pojoList.size();
    }
}
