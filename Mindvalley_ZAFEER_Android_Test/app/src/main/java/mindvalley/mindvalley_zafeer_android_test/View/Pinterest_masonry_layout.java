package mindvalley.mindvalley_zafeer_android_test.View;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Zaf on 08/10/2016.
 */

public class Pinterest_masonry_layout extends RecyclerView.ItemDecoration {

    private final int mSpace;

    public Pinterest_masonry_layout(int space){

        this.mSpace = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = mSpace;
        outRect.right = mSpace;
        outRect.bottom = mSpace;

        if(parent.getChildAdapterPosition(view) == 0){
            outRect.top=mSpace;
        }
//        super.getItemOffsets(outRect, view, parent, state);
    }
}
