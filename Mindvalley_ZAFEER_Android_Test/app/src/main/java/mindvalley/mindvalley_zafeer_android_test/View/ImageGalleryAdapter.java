package mindvalley.mindvalley_zafeer_android_test.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import mindvalley.mindvalley_zafeer_android_test.Controller.MainActivity;
import mindvalley.mindvalley_zafeer_android_test.Controller.User_profile;
import mindvalley.mindvalley_zafeer_android_test.Controller.VolleyController;
import mindvalley.mindvalley_zafeer_android_test.Model.POJO;
import mindvalley.mindvalley_zafeer_android_test.R;
import static mindvalley.mindvalley_zafeer_android_test.R.drawable.warning;

/**
 * Created by Zaf on 07/10/2016.
 */

public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.MyViewHolder> {

    private List<POJO> mImages;
    public Context mContext;
    private ImageLoader.ImageContainer mImageContainer;


    public ImageGalleryAdapter(Context context, List<POJO> images){
        mContext = context;
        this.mImages = images;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_thumbnail;
        ImageView pro_pic;
        TextView username;
        TextView likes;


        public MyViewHolder(View itemView) {
            super(itemView);

            img_thumbnail = (ImageView)itemView.findViewById(R.id.Image_thumbnail);
            pro_pic = (ImageView)itemView.findViewById(R.id.profile_pic);
            username = (TextView)itemView.findViewById(R.id.tvName);
            likes = (TextView)itemView.findViewById(R.id.Nlove);
        }
    }

    @Override
    public ImageGalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.thumbnail_image_gallery
                            ,parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ImageGalleryAdapter.MyViewHolder holder, int position) {

        ImageLoader imageLoader = VolleyController.getInstance().getImageLoader();
        final POJO thumb_img = mImages.get(position);
        final POJO pro_pic = mImages.get(position);
        final POJO username = mImages.get(position);
        POJO likes = mImages.get(position);

        final String TAG = MainActivity.class.getSimpleName();


        imageLoader.get(thumb_img.getUrls().getThumb(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null){
                    holder.img_thumbnail.setImageBitmap(response.getBitmap());
                }else if(thumb_img.getUrls().getThumb() == null){
                   mImageContainer.cancelRequest();
                    mImageContainer = null;
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
                holder.img_thumbnail.setImageResource(R.drawable.warning);
            }
        });

        imageLoader.get(pro_pic.getUser().getProfileImage().getSmall(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null){
                    holder.pro_pic.setImageBitmap(response.getBitmap());
                    holder.pro_pic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(mContext, "Clicked: " +username.getUser().getName(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }else if (pro_pic.getUser().getProfileImage().getSmall() == null){
                    mImageContainer.cancelRequest();
                    mImageContainer = null;
                }

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Image Load Error: " + error.getMessage());
            }
        });

       holder.username.setText(username.getUser().getUsername());
        holder.likes.setText(Integer.toString(likes.getLikes()));
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public interface ClickListener{
        void OnClick(View v, int position);

        void onLongClick(View v, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ImageGalleryAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView,
                                     final ImageGalleryAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;

            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e) {

            View child = recyclerView.findChildViewUnder(e.getX(), e.getY());

            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.OnClick(child, recyclerView.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
